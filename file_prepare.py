import requests
import pymorphy2


def preparing(word_len):
    morph = pymorphy2.MorphAnalyzer()
    response = requests.get('https://raw.githubusercontent.com/danakt/russian-words/master/russian.txt')

    text = response.content.decode('cp1251')
    with open('russian.txt', 'wb') as ru:
        ru.write(text.encode('utf-8'))

    filename = 'rus_prepare_' + str(word_len) + '.txt'
    with open('russian.txt') as rus_all, open(filename, 'w+') as rus_prep:
        for word in rus_all:
            word = word.rstrip()
            if len(word) == word_len:
                if {'NOUN', 'nomn', 'sing'} in morph.parse(word)[0].tag:
                    rus_prep.write(word + '\n')


if __name__ == '__main__':
    preparing(word_len=5)
