import os.path
import re

import file_prepare


def iteration_search(filename, amount_of_chars, right_chars, regex, unused_chars, swap_chars, words):
    print('Enter word')
    word = str(input())
    print('Enter number of chars on their places or 0')
    right_places = list(map(int, input().split()))

    # chars on right places according to first word
    if right_places[0] != 0:
        for i in right_places:
            right_chars.append(word[i - 1])

    print('Enter chars on wrong places or 0')
    swap_chars += list(input().split())

    # creating list of unused chars
    unused_chars += list(word)
    for char in swap_chars:
        if char in unused_chars:
            unused_chars.remove(char)
    for char in right_chars:
        if char in unused_chars:
            unused_chars.remove(char)

    # creating regex
    if right_places[0] != 0:
        for i in right_places:
            if regex[i] != '.':
                regex[i] = right_chars[i]

    # All data for search words
    print(f'Unused letters: {unused_chars}\nLetters on their places: {right_chars}')
    print(f'Additional letters: {swap_chars}\nRegex: {regex}')

    empty_regex = '^'
    for i in range(amount_of_chars):
        empty_regex += '.'
    empty_regex += '$'

    # TODO preparing list from file
    with open(filename) as dictionary:
        for line in dictionary:
            line = line.rstrip()
            if (not any(x in line for x in unused_chars)) and (any(x in line for x in swap_chars)):
                words.append(line)

    answers = []
    if regex != empty_regex:
        for word in words:
            answers.append(re.search(regex, word).string)
    else:
        answers = words

    print(answers)
    print('Next iteration of search. Push ctrl+C to stop app.')
    iteration_search(filename, amount_of_chars, right_chars, regex, unused_chars, swap_chars, words)


def main():
    print('Enter number of chars')
    amount_of_chars = int(input())

    # preparing library file
    filename = 'rus_prepare_' + str(amount_of_chars) + '.txt'
    if not os.path.exists(filename):
        print('Dictionary is not ready. Downloading...')
        file_prepare.preparing(word_len=amount_of_chars)
        print('Done!')

    empty_regex = '^'
    for i in range(amount_of_chars):
        empty_regex += '.'
    empty_regex += '$'

    iteration_search(filename, amount_of_chars, '', empty_regex, [], [], [])


if __name__ == '__main__':
    main()
